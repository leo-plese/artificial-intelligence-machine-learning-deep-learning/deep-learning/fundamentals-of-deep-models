from sklearn.svm import SVC
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import hinge_loss
import torch
import data
import pt_deep

class KSVMWrap():
    '''
    Metode:
      __init__(self, X, Y_, param_svm_c=1, param_svm_gamma='auto'):
        Konstruira omotač i uči RBF SVM klasifikator
        X, Y_:           podatci i točni indeksi razreda
        param_svm_c:     relativni značaj podatkovne cijene
        param_svm_gamma: širina RBF jezgre

      predict(self, X)
        Predviđa i vraća indekse razreda podataka X

      get_scores(self, X):
        Vraća klasifikacijske mjere
        (engl. classification scores) podataka X;
        ovo će vam trebati za računanje prosječne preciznosti.

      support
        Indeksi podataka koji su odabrani za potporne vektore
    '''
    def __init__(self, X, Y_, param_svm_c=1, param_svm_gamma='auto'):
        self.clf = SVC(kernel='rbf', C=param_svm_c, gamma=param_svm_gamma)
        self.clf.fit(X, Y_)

    def predict(self, X):
        return self.clf.predict(X)

    def get_scores(self, X):
        return self.clf.decision_function(X)

    def support(self):
        return self.clf.support_

    def get_accuracy_score(self, X, Y_):
        return self.clf.score(X, Y_)


def binlogreg_decfun(ksvm):
    def classify(X):
        return ksvm.get_scores(X)

    return classify

if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)

    # bolje bez whiteninga (KSVM)
    # bolje sa whiteningom (DEEP)


    inv_lambda_list = [1, 1e1, 1e2, 1e3]
    CN_list = [(2, 100)]
    KCN_list = [(4, 2, 40), (6, 2, 10)]

    layers_width_list = [[2, 2], [2, 10, 2], [2, 10, 10, 2]]
    f_activ_dict = {'relu':torch.relu, 'sigmoid':torch.sigmoid}

    ds_list = CN_list+KCN_list

    for ds in ds_list:
        np.random.seed(100)
        torch.manual_seed(10)

        print()
        print("/////////////////////////////////////////////")

        if len(ds) == 2:
            C, N = ds
            K = 1

            print("C N =", C, N)
            X, Y_ = data.sample_gauss_2d(C, N)
        else:
            K, C, N = ds

            print("K C N =", K, C, N)
            X, Y_ = data.sample_gmm_2d(K, C, N)


        #################### SVM ##################################
        for inv_lambda in inv_lambda_list:
            print()
            print("C =", inv_lambda)

            ksvm = KSVMWrap(X, Y_, param_svm_c=inv_lambda)

            scores = ksvm.get_scores(X)

            print("Hinge loss =", hinge_loss(Y_.flatten(), scores))

            # evaluate and print performance measures
            # prediktirani indeksi razreda podataka X
            if K == 1:
                Y = ksvm.predict(X).reshape(C*N, -1)
            else:
                Y = ksvm.predict(X).reshape(K*N, -1)



            accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
            sorted_Y_by_pred_probs = Y_[scores.argsort(axis=0)].flatten()
            AP = data.eval_AP(sorted_Y_by_pred_probs)
            print("accuracy =", accuracy, "\t("+str(ksvm.get_accuracy_score(X,Y_))+")")
            print("recall =", recall)
            print("precision =", precision)
            print("AP =", AP)

            # iscrtaj rezultate, decizijsku plohu
            decfun = binlogreg_decfun(ksvm)
            bbox = (np.min(X, axis=0), np.max(X, axis=0))
            data.graph_surface(decfun, bbox, offset=0)

            # graph the data points
            data.graph_data(X, Y_, Y, special=ksvm.support())

            plt.title("KSVM (C = " + str(inv_lambda) + ") ; A = " + str(accuracy) + ", P = " + str(recall) + ", R = " + str(precision) + " AP =" + str(AP))
            # show the plot
            plt.show()


        X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)
        ############ DEEP ################
        Yoh_ = data.get_one_hot_matrix(Y_.flatten(), len(X), (max(Y_) + 1)[0])

        X_tensor = torch.tensor(X, dtype=torch.float32)
        Yoh_tensor = torch.tensor(Yoh_, dtype=torch.float32)

        for layers_width in layers_width_list:
            print()
            print("layers_width =", layers_width)

            for f_activ_k in f_activ_dict:
                f_activ = f_activ_dict[f_activ_k]
                np.random.seed(100)
                torch.manual_seed(10)

                print("--", f_activ)
                ptd = pt_deep.PTDeep(layers_width, f_activ)
                num_params = ptd.count_params()
                print("#params =", num_params)
                W, b, loss = pt_deep.train(ptd, X_tensor, Yoh_tensor, 1000, 0.5, 0)

                print("CE loss =", loss)

                # dohvati vjerojatnosti na skupu za učenje
                if K == 1:
                    probs = pt_deep.eval(ptd, X)[:, 1].reshape(C * N, -1)
                else:
                    probs = pt_deep.eval(ptd, X)[:, 1].reshape(K * N, -1)

                # ispiši performansu (preciznost i odziv po razredima)
                # recover the predicted classes Y
                Y = (probs >= 0.5)

                # evaluate and print performance measures
                accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
                sorted_Y_by_pred_probs = Y_[probs.argsort(axis=0)[:, 0]].flatten()
                AP = data.eval_AP(sorted_Y_by_pred_probs)
                print("accuracy =", accuracy)
                print("recall =", recall)
                print("precision =", precision)
                print("AP =", AP)

                # iscrtaj rezultate, decizijsku plohu
                decfun = pt_deep.binlogreg_decfun(ptd)
                bbox = (np.min(X, axis=0), np.max(X, axis=0))
                data.graph_surface(decfun, bbox, offset=0.5, draw_cmap_multi=True)

                # graph the data points
                data.graph_data(X, Y_, Y)

                # show the plot
                plt.title("DEEP "+ str(layers_width) + " ("+f_activ_k + ") ; A = " + str(accuracy) + ", P = " + str(recall) + ", R = " + str(precision) + " AP =" + str(AP))
                plt.show()
