import torch
import torchvision
import matplotlib.pyplot as plt
import numpy as np
from sklearn.svm import SVC
import pt_deep
import data


def evaluate_model(ptd, N, C, X, Y_):
    # dohvati vjerojatnosti na skupu za učenje
    probs = pt_deep.eval(ptd, X)

    # ispiši performansu (preciznost i odziv po razredima)
    # recover the predicted classes Y
    Y = np.argmax(probs, axis=1).reshape((N, -1))

    # # evaluate and print performance measures
    conf_mats_by_cls, _, accuracy_avg, precision_avg, recall_avg, _, precision_by_cls, _ = data.eval_perf_multi(
        Y, Y_, N, C)

    return accuracy_avg, precision_avg, recall_avg


if __name__ == "__main__":
    dataset_root = r".\data"  # change this to your preference
    mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=False)
    mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=False)

    x_train, y_train = mnist_train.data, mnist_train.targets
    x_test, y_test = mnist_test.data, mnist_test.targets
    # print(x_train.max().item(), x_test.max().item())
    x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)
    # print(x_train.max().item(), x_test.max().item())

    N = x_train.shape[0]
    N_test = x_test.shape[0]
    D1, D2 = x_train.shape[1], x_train.shape[2]
    D = D1 * D2
    C = y_train.max().add_(1).item()
    print()

    ###############
    Yoh_ = torch.tensor(data.get_one_hot_matrix(y_train.flatten(), N, C))

    LAMBDAS = [0, 1e-4, 1e-3, 1e-2, 1e-1]
    layers_width_list = [[784, 10], [784, 100, 10], [784, 100, 100, 10], [784, 100, 100, 100, 10]]

    # # zad 1
    plt.rcParams["figure.figsize"] = (20, 20)
    for reg_factor in LAMBDAS:
        np.random.seed(100)
        torch.manual_seed(10)

        layers_width = layers_width_list[0]
        ptd = pt_deep.PTDeep(layers_width, torch.relu).cuda()
        num_params = ptd.count_params()
        print("#params =", num_params)
        # lr=0.5: 1e-2 dosta dobro, 1e-1 dosta dobro
        # --->> lr=0.3: 1e-2 dobro, 1e-1 dosta dobro
        # lr=0.1: 1e-2 pocinje se nazirati, 1e-1 dosta dobro

        W, b, loss = pt_deep.train(ptd, x_train.reshape(N, -1).cuda(), Yoh_.cuda(), 1000, 0.1, reg_factor)

        fig, axs = plt.subplots(2, 5)
        W = np.array(W).reshape(C, D1, D2)
        di = 1
        for wi in W:
            plt.subplot(2, 5, di)
            plt.imshow(wi, cmap=plt.get_cmap('gray'))

            di += 1

        plt.suptitle("DEEP " + str(layers_width) + " (relu); lambda = " + str(reg_factor))
        plt.show()

    # zad 2, 3, 4
    #### VALIDACIJA ####
    train_deep = False  # True # True for training, False for drawing
    SAVE_LOSS_BASE_PATH = r"..\deep_losses"
    SAVE_TRAIN_LOSS_BASE_PATH = SAVE_LOSS_BASE_PATH + "\\train"
    SAVE_VAL_LOSS_BASE_PATH = SAVE_LOSS_BASE_PATH + "\\val"
    SAVE_A_P_R_BASE_PATH = r"..\deep_apr"
    SAVE_A_P_R_BASE_TRAIN_PATH = SAVE_A_P_R_BASE_PATH + "\\train"
    SAVE_A_P_R_BASE_TEST_PATH = SAVE_A_P_R_BASE_PATH + "\\test"

    n_iter_list = [6000, 9000, 12000, 15000]
    lr_list = [0.1, 0.09, 0.06, 0.04]

    if train_deep:
        val_to_ind = int(0.2 * N)
        perm = np.random.permutation(N)
        x_val, y_val = x_train[perm][:val_to_ind], y_train[perm][:val_to_ind]
        x_train_new, y_train_new = x_train[perm][val_to_ind:], y_train[perm][val_to_ind:]
        Yoh_val, Yoh_train_new = Yoh_[perm][:val_to_ind], Yoh_[perm][val_to_ind:]
        N_train_new, N_val = len(x_train_new), len(x_val)

        for i in range(len(layers_width_list)):
            layers_width = layers_width_list[i]
            lr = lr_list[i]
            n_iter = n_iter_list[i]
            print()
            print()
            print()
            print("############################", layers_width, " ############################")
            for reg_factor in LAMBDAS:
                print()
                print("*************** lambda =", reg_factor, "***************")

                ptd = pt_deep.PTDeep(layers_width, torch.relu).cuda()

                x_train_go, Yoh_go = x_train_new.reshape(N_train_new, -1).cuda(), Yoh_train_new.cuda()
                W, b, loss = pt_deep.train(ptd, x_train_go, Yoh_go, n_iter, lr, reg_factor, validate=True,
                                           X_val=x_val.reshape(N_val, -1).cuda(), y_val=y_val, Yoh_val=Yoh_val.cuda(),
                                           save_loss_path=(
                                               SAVE_TRAIN_LOSS_BASE_PATH + "\\" + str(len(layers_width)) + "_" + str(
                                                   reg_factor) + ".txt",
                                               SAVE_VAL_LOSS_BASE_PATH + "\\" + str(len(layers_width)) + "_" + str(
                                                   reg_factor) + ".txt"))

                A_train, P_train, R_train = evaluate_model(ptd, N, C, x_train.reshape(N, -1).cuda(), y_train)

                A_test, P_test, R_test = evaluate_model(ptd, N_test, C, x_test.reshape(N_test, -1).cuda(), y_test)

                print()
                print("loss = ", loss)
                print("TRAIN: A = {}, P = {}, R = {}".format(A_train, P_train, R_train))
                print("TEST: A = {}, P = {}, R = {}".format(A_test, P_test, R_test))

                np.savetxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".out",
                           np.array([A_train, P_train, R_train]))
                np.savetxt(SAVE_A_P_R_BASE_TEST_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".out",
                           np.array([A_test, P_test, R_test]))
    else:
        # load A, P, R
        A_train_list_all, A_test_list_all, P_train_list_all, P_test_list_all, R_train_list_all, R_test_list_all = [], [], [], [], [], []
        for i in range(len(layers_width_list)):
            layers_width = layers_width_list[i]
            lr = lr_list[i]
            n_iter = n_iter_list[i]

            A_train_list, A_test_list, P_train_list, P_test_list, R_train_list, R_test_list = [], [], [], [], [], []
            for reg_factor in LAMBDAS:
                APR_train = np.loadtxt(
                    SAVE_A_P_R_BASE_TRAIN_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
                A_train, P_train, R_train = APR_train[0], APR_train[1], APR_train[2]
                APR_test = np.loadtxt(
                    SAVE_A_P_R_BASE_TEST_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
                A_test, P_test, R_test = APR_test[0], APR_test[1], APR_test[2]

                A_train_list.append(A_train)
                A_test_list.append(A_test)
                P_train_list.append(P_train)
                P_test_list.append(P_test)
                R_train_list.append(R_train)
                R_test_list.append(R_test)

            A_train_list_all.append(A_train_list)
            A_test_list_all.append(A_test_list)
            P_train_list_all.append(P_train_list)
            P_test_list_all.append(P_test_list)
            R_train_list_all.append(R_train_list)
            R_test_list_all.append(R_test_list)

        best_lambda_for_APR_per_model = np.argmax(A_test_list_all, axis=1)

        print()
        print("************* Train loss *************")
        cur_model_train_loss_list = []
        for i in range(len(layers_width_list)):
            layers_width = layers_width_list[i]
            lr = lr_list[i]
            n_iter = n_iter_list[i]

            for reg_factor in LAMBDAS:
                train_loss_for_reg_factor = np.loadtxt(
                    SAVE_TRAIN_LOSS_BASE_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")

                if reg_factor == LAMBDAS[best_lambda_for_APR_per_model[i]]:
                    cur_model_train_loss_list.append((reg_factor, train_loss_for_reg_factor))

                plt.plot(range(n_iter), train_loss_for_reg_factor, label=str(reg_factor))

            plt.legend(loc="best")
            plt.title("Train loss: "+str(layers_width))
            plt.xlabel("n iter")
            plt.ylabel("loss")
            plt.ylim(0,20)
            plt.show()


        for i in range(len(cur_model_train_loss_list)):
            plt.plot(range(n_iter_list[i]), cur_model_train_loss_list[i][1], label="{} (lambda = {})".format(layers_width_list[i],cur_model_train_loss_list[i][0]))
        plt.legend(loc="best")
        plt.title("Train loss for best lambda per model")
        plt.xlabel("n iter")
        plt.ylabel("loss")
        plt.show()

        print()
        print("************* APR *************")


        colors = ["green", "blue", "red", "black"]
        print()
        print("Accuracy")
        for i in range(len(layers_width_list)):
            layers_width = layers_width_list[i]
            lr = lr_list[i]
            n_iter = n_iter_list[i]

            plt.plot(range(len(LAMBDAS)), A_train_list_all[i], color=colors[i], linestyle="--", label="{} train".format(layers_width))
            plt.plot(range(len(LAMBDAS)), A_test_list_all[i], color=colors[i], linestyle="-", label="{} test".format(layers_width))
        plt.xticks(range(len(LAMBDAS)), LAMBDAS)
        plt.xlabel("lambda")
        plt.ylabel("accuracy")
        plt.title("Accuracy")
        plt.legend(loc="best")
        plt.show()

        print()
        print("Precision")
        for i in range(len(layers_width_list)):
            layers_width = layers_width_list[i]
            lr = lr_list[i]
            n_iter = n_iter_list[i]

            plt.plot(range(len(LAMBDAS)), P_train_list_all[i], color=colors[i], linestyle="--", label="{} train".format(layers_width))
            plt.plot(range(len(LAMBDAS)), P_test_list_all[i], color=colors[i], linestyle="-", label="{} test".format(layers_width))
        plt.xticks(range(len(LAMBDAS)), LAMBDAS)
        plt.xlabel("lambda")
        plt.ylabel("precision")
        plt.title("Precision")
        plt.legend(loc="best")
        plt.show()

        print()
        print("Recall")
        for i in range(len(layers_width_list)):
            layers_width = layers_width_list[i]
            lr = lr_list[i]
            n_iter = n_iter_list[i]

            plt.plot(range(len(LAMBDAS)), R_train_list_all[i], color=colors[i], linestyle="--", label="{} train".format(layers_width))
            plt.plot(range(len(LAMBDAS)), R_test_list_all[i], color=colors[i], linestyle="-", label="{} test".format(layers_width))
        plt.xticks(range(len(LAMBDAS)), LAMBDAS)
        plt.xlabel("lambda")
        plt.ylabel("recall")
        plt.title("Recall")
        plt.legend(loc="best")
        plt.show()

        #############

        # deal with the best model
        train_best = True
        # draw data which contribute loss at most
        draw_data_most_contrib_loss = False
        # validate best model and find best validation performance
        validate = False
        validate_mb = False  # False if "normal" train (with validation)
        optimizer_adam = True  # False for SGD
        best_batch_size = (64, 16)  # used in Adam for comparison with SGD optimizer
        best_patience = 100  # used in Adam for comparison with SGD optimizer
        variable_lr = True  # for Adam
        lr_adam = 1e-4

        show_random_init_loss = False  # False

        train_svm = False    # needs validate = False

        # train the best config once more and draw the data which contribute to the loss at most
        if train_best:
            # get best model of all tested
            A_test_list_all = np.array(A_test_list_all)
            model_i, reg_factor_i = np.unravel_index(A_test_list_all.argmax(), A_test_list_all.shape)

            # best model - get hyperparams
            n_iter = n_iter_list[model_i] if not validate else 200000  # 12000

            lr = lr_list[model_i]  # 0.06
            reg_factor = LAMBDAS[reg_factor_i]  # 1e-3
            layers_width = layers_width_list[model_i]  # [784, 100, 100, 10]

            print()
            print("BEST model:")
            print("arch =", layers_width)
            print("n iter =", n_iter)
            print("lr =", lr)
            print("lambda =", reg_factor)
            print()

            if draw_data_most_contrib_loss:
                plt.rcParams["figure.figsize"] = (20, 20)

                ptd = pt_deep.PTDeep(layers_width, torch.relu).cuda()
                x_train_go, Yoh_go = x_train.reshape(N, -1).cuda(), Yoh_.cuda()
                W, b, loss, data_loss_contrib_desc_order = pt_deep.train(ptd, x_train_go, Yoh_go, n_iter, lr,
                                                                         reg_factor, get_data_loss_contrib=True)
                X_ordered_by_loss_contrib_dec, y_train_ordered_by_loss_contrib_dec = x_train.detach().numpy()[
                                                                                         data_loss_contrib_desc_order], \
                                                                                     y_train.detach().numpy()[
                                                                                         data_loss_contrib_desc_order]
                top10_data_for_loss, top10_y_for_loss = X_ordered_by_loss_contrib_dec[
                                                        :10], y_train_ordered_by_loss_contrib_dec[:10]

                di = 1
                for xi, yi in zip(top10_data_for_loss, top10_y_for_loss):
                    plt.subplot(2, 5, di)
                    plt.title("{}. (gt = {})".format(di, yi))
                    plt.imshow(xi, cmap=plt.get_cmap('gray'))
                    di += 1
                plt.show()

                A_train, P_train, R_train = evaluate_model(ptd, N, C, x_train.reshape(N, -1).cuda(), y_train)
                A_test, P_test, R_test = evaluate_model(ptd, N_test, C, x_test.reshape(N_test, -1).cuda(), y_test)

                print()
                print("loss = ", loss)
                print("TRAIN: A = {}, P = {}, R = {}".format(A_train, P_train, R_train))
                print("TEST: A = {}, P = {}, R = {}".format(A_test, P_test, R_test))

            if validate:
                print("n iter =", n_iter)
                val_to_ind = int(0.2 * N)
                perm = np.random.permutation(N)
                x_val, y_val = x_train[perm][:val_to_ind], y_train[perm][:val_to_ind]
                x_train_new, y_train_new = x_train[perm][val_to_ind:], y_train[perm][val_to_ind:]
                Yoh_val, Yoh_train_new = Yoh_[perm][:val_to_ind], Yoh_[perm][val_to_ind:]
                N_train_new, N_val = len(x_train_new), len(x_val)

                ptd = pt_deep.PTDeep(layers_width, torch.relu).cuda()

                x_train_go, Yoh_go = x_train_new.reshape(N_train_new, -1).cuda(), Yoh_train_new.cuda()

                if not validate_mb:
                    W, b, loss = pt_deep.train(ptd, x_train_go, Yoh_go, n_iter, lr, reg_factor, validate=True,
                                               X_val=x_val.reshape(N_val, -1).cuda(), y_val=y_val,
                                               Yoh_val=Yoh_val.cuda(),
                                               save_loss_path=(
                                                   SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_" + str(
                                                       len(layers_width)) + "_" + str(reg_factor) + ".txt",
                                                   SAVE_VAL_LOSS_BASE_PATH + "\\BEST_" + str(
                                                       len(layers_width)) + "_" + str(reg_factor) + ".txt"),
                                               patience=7)
                else:
                    # tested on:
                    # train_batch_size=8, val_batch_size=2
                    # train_batch_size = 16, val_batch_size = 4
                    # train_batch_size = 32, val_batch_size = 8
                    # train_batch_size = 64, val_batch_size = 16
                    # train_batch_size = 128, val_batch_size = 32
                    # train_batch_size = 256, val_batch_size = 64
                    # train_batch_size = 1024, val_batch_size = 256

                    # patience = 7, 100

                    if not optimizer_adam:
                        W, b, loss = pt_deep.train_mb(ptd, x_train_go, Yoh_go, n_iter, lr, reg_factor,
                                                      train_batch_size=64, val_batch_size=16,
                                                      X_val=x_val.reshape(N_val, -1).cuda(), y_val=y_val,
                                                      Yoh_val=Yoh_val.cuda(),
                                                      save_loss_path=(
                                                          SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt",
                                                          SAVE_VAL_LOSS_BASE_PATH + "\\BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt"),
                                                      patience=100)
                    else:
                        filename_ext = "_var_lr" if variable_lr else "_const_lr_" + str(lr_adam)
                        W, b, loss = pt_deep.train_mb(ptd, x_train_go, Yoh_go, n_iter, param_delta=lr_adam,
                                                      param_lambda=reg_factor,
                                                      train_batch_size=best_batch_size[0],
                                                      val_batch_size=best_batch_size[1],
                                                      X_val=x_val.reshape(N_val, -1).cuda(), y_val=y_val,
                                                      Yoh_val=Yoh_val.cuda(),
                                                      save_loss_path=(
                                                          SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_MB\\adam\\" + str(best_batch_size[0]) + "_" + str(best_batch_size[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + filename_ext + ".txt",
                                                          SAVE_VAL_LOSS_BASE_PATH + "\\BEST_MB\\adam\\" + str(best_batch_size[0]) + "_" + str(best_batch_size[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + filename_ext + ".txt"),
                                                      patience=best_patience,
                                                      optimizer="adam",
                                                      variable_lr=variable_lr)

                    A_train, P_train, R_train = evaluate_model(ptd, N, C, x_train.reshape(N, -1).cuda(), y_train)
                    A_test, P_test, R_test = evaluate_model(ptd, N_test, C, x_test.reshape(N_test, -1).cuda(), y_test)

                    print()
                    print("loss = ", loss)
                    print("TRAIN: A = {}, P = {}, R = {}".format(A_train, P_train, R_train))
                    print("TEST: A = {}, P = {}, R = {}".format(A_test, P_test, R_test))

                    if not optimizer_adam:
                        np.savetxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_MB_" + str(len(layers_width)) + "_" + str(
                            reg_factor) + ".out", np.array([A_train, P_train, R_train]))
                        np.savetxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_MB_" + str(len(layers_width)) + "_" + str(
                            reg_factor) + ".out", np.array([A_test, P_test, R_test]))
                    else:
                        np.savetxt(
                            SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_MB\\adam\\" + str(best_batch_size[0]) + "_" + str(
                                best_batch_size[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(
                                reg_factor) + filename_ext + ".out", np.array([A_train, P_train, R_train]))
                        np.savetxt(
                            SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_MB\\adam\\" + str(best_batch_size[0]) + "_" + str(
                                best_batch_size[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(
                                reg_factor) + filename_ext + ".out", np.array([A_test, P_test, R_test]))

            elif train_svm:
                train_kernel = "linear"    # "rbf"

                svm_rbf = SVC(kernel=train_kernel, C=1 / reg_factor, gamma='auto', decision_function_shape='ovo')

                x_train_resh, x_test_resh = x_train.reshape(N, -1), x_test.reshape(N_test, -1)
                print("TRAIN")
                svm_rbf.fit(x_train_resh, y_train)

                print("PREDICT train")
                y_train_preds = svm_rbf.predict(x_train_resh)

                print("EVAL train")
                _, _, A_train, P_train, R_train, _, _, _ = data.eval_perf_multi(y_train_preds, y_train, N, C)

                print("Test")
                print("PREDICT test")
                y_test_preds = svm_rbf.predict(x_test_resh)

                print("EVAL test")
                _, _, A_test, P_test, R_test, _, _, _ = data.eval_perf_multi(y_test_preds, y_test, N_test, C)

                np.savetxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_SVM_" + train_kernel+ "_"+str(len(layers_width)) + "_" + str(reg_factor) + ".out", np.array([A_train, P_train, R_train]))
                np.savetxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_SVM_"  + train_kernel+ "_"+ str(len(layers_width)) + "_" + str(reg_factor) + ".out", np.array([A_test, P_test, R_test]))

            # ############# plot train/val loss and print A P R of early stopping validated best model #################

            APR_best_train = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_train, P_best_train, R_best_train = APR_best_train[0], APR_best_train[1], APR_best_train[2]
            APR_best_test = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_test, P_best_test, R_best_test = APR_best_test[0], APR_best_test[1], APR_best_test[2]

            APR_train = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_train, P_train, R_train = APR_train[0], APR_train[1], APR_train[2]
            APR_test = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_test, P_test, R_test = APR_test[0], APR_test[1], APR_test[2]

            print()
            print("Performance (A, P, R)...")
            print("TRAIN (best | initial):")
            print("A = {} | {}".format(A_best_train, A_train))
            print("P = {} | {}".format(P_best_train, P_train))
            print("R = {} | {}".format(R_best_train, R_train))

            print("TEST (best | initial):")
            print("A = {} | {}".format(A_best_test, A_test))
            print("P = {} | {}".format(P_best_test, P_test))
            print("R = {} | {}".format(R_best_test, R_test))


            best_n_iter = 60012
            init_n_iter = 12000

            train_loss_for_reg_factor = np.loadtxt(SAVE_TRAIN_LOSS_BASE_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")
            val_loss_for_reg_factor = np.loadtxt(SAVE_VAL_LOSS_BASE_PATH + "\\" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")

            best_train_loss_for_reg_factor = np.loadtxt(SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")
            best_val_loss_for_reg_factor = np.loadtxt(SAVE_VAL_LOSS_BASE_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")

            print()
            print("Loss...")
            print("TRAIN (best | initial)")
            print("{} | {}".format(best_train_loss_for_reg_factor[best_n_iter], train_loss_for_reg_factor[init_n_iter-1]))

            print("VAL (best | initial)")
            print("{} | {}".format(best_val_loss_for_reg_factor[best_n_iter], val_loss_for_reg_factor[init_n_iter - 1]))

            plt.plot(range(len(train_loss_for_reg_factor)), train_loss_for_reg_factor, color="blue",linestyle="--", label="train")
            plt.plot(range(len(val_loss_for_reg_factor)), val_loss_for_reg_factor, color="blue",linestyle="-", label="val")
            plt.plot(range(len(best_train_loss_for_reg_factor)), best_train_loss_for_reg_factor, color="red", linestyle="--", label="best train")
            plt.plot(range(len(best_val_loss_for_reg_factor)), best_val_loss_for_reg_factor, color="red", linestyle="-", label="best val")

            plt.axvline(x=best_n_iter, color="green", label="best n iter")
            plt.legend(loc="best")
            plt.title("Train/Val loss: " + str(layers_width))
            plt.xlabel("n iter")
            plt.ylabel("loss")
            plt.show()

            # ############### plot train/val loss and print A P R of TRAIN MB validated best model ###############
            train_val_batchsize_list = [(8, 2), (16, 4), (32, 8), (64, 16), (128, 32), (256, 64), (1024, 256)]

            # APR
            A_pat_7_train_list, P_pat_7_train_list, R_pat_7_train_list = [], [], []
            A_pat_100_train_list, P_pat_100_train_list, R_pat_100_train_list = [], [], []
            A_pat_7_test_list, P_pat_7_test_list, R_pat_7_test_list = [], [], []
            A_pat_100_test_list, P_pat_100_test_list, R_pat_100_test_list = [], [], []
            for train_val_batchsize in train_val_batchsize_list:
                APR_pat7_best_train = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_MB\\patience7\\"+str(train_val_batchsize[0])+"_"+str(train_val_batchsize[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
                APR_pat100_best_train = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_MB\\patience100\\" + str(train_val_batchsize[0]) + "_" + str(train_val_batchsize[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")

                A_pat7_best_train, P_pat7_best_train, R_pat7_best_train = APR_pat7_best_train[0], APR_pat7_best_train[1], APR_pat7_best_train[2]
                A_pat100_best_train, P_pat100_best_train, R_pat100_best_train = APR_pat100_best_train[0], APR_pat100_best_train[1], APR_pat100_best_train[2]
                A_pat_7_train_list.append(A_pat7_best_train)
                P_pat_7_train_list.append(A_pat7_best_train)
                R_pat_7_train_list.append(A_pat7_best_train)
                A_pat_100_train_list.append(A_pat100_best_train)
                P_pat_100_train_list.append(A_pat100_best_train)
                R_pat_100_train_list.append(A_pat100_best_train)

                APR_pat7_best_test = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_MB\\patience7\\"+str(train_val_batchsize[0])+"_"+str(train_val_batchsize[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
                APR_pat100_best_test = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_MB\\patience100\\" + str(train_val_batchsize[0]) + "_" + str(train_val_batchsize[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")

                A_pat7_best_test, P_pat7_best_test, R_pat7_best_test = APR_pat7_best_test[0], APR_pat7_best_test[1], APR_pat7_best_test[2]
                A_pat100_best_test, P_pat100_best_test, R_pat100_best_test = APR_pat100_best_test[0], APR_pat100_best_test[1], APR_pat100_best_test[2]
                A_pat_7_test_list.append(A_pat7_best_test)
                P_pat_7_test_list.append(A_pat7_best_test)
                R_pat_7_test_list.append(A_pat7_best_test)
                A_pat_100_test_list.append(A_pat100_best_test)
                P_pat_100_test_list.append(A_pat100_best_test)
                R_pat_100_test_list.append(A_pat100_best_test)

            print()
            print("Performance (A, P, R)...")

            colors = ["blue", "red", "green", "yellow", "gray", "orange", "black"]
            print()
            print("Accuracy")

            plt.plot(range(len(train_val_batchsize_list)), A_pat_7_train_list, color=colors[0], linestyle="--", label="{} train".format(7))
            plt.plot(range(len(train_val_batchsize_list)), A_pat_7_test_list, color=colors[0], linestyle="-", label="{} test".format(7))
            plt.plot(range(len(train_val_batchsize_list)), A_pat_100_train_list, color=colors[1], linestyle="-.", label="{} train".format(100))
            plt.plot(range(len(train_val_batchsize_list)), A_pat_100_test_list, color=colors[1], linestyle=":", label="{} test".format(100))
            plt.xticks(range(len(train_val_batchsize_list)), train_val_batchsize_list)
            plt.xlabel("train/val batchsize")
            plt.ylabel("accuracy")
            plt.title("Accuracy")
            plt.legend(loc="best")
            plt.show()

            print()
            print("Precision")
            plt.plot(range(len(train_val_batchsize_list)), P_pat_7_train_list, color=colors[0], linestyle="--", label="{} train".format(7))
            plt.plot(range(len(train_val_batchsize_list)), P_pat_7_test_list, color=colors[0], linestyle="-", label="{} test".format(7))
            plt.plot(range(len(train_val_batchsize_list)), P_pat_100_train_list, color=colors[1], linestyle="-.", label="{} train".format(100))
            plt.plot(range(len(train_val_batchsize_list)), P_pat_100_test_list, color=colors[1], linestyle=":", label="{} test".format(100))
            plt.xticks(range(len(train_val_batchsize_list)), train_val_batchsize_list)
            plt.xlabel("train/val batchsize")
            plt.ylabel("precision")
            plt.title("Precision")
            plt.legend(loc="best")
            plt.show()

            print()
            print("Recall")
            plt.plot(range(len(train_val_batchsize_list)), R_pat_7_train_list, color=colors[0], linestyle="--",
                     label="{} train".format(7))
            plt.plot(range(len(train_val_batchsize_list)), R_pat_7_test_list, color=colors[0], linestyle="-",
                     label="{} test".format(7))
            plt.plot(range(len(train_val_batchsize_list)), R_pat_100_train_list, color=colors[1], linestyle="-.",
                     label="{} train".format(100))
            plt.plot(range(len(train_val_batchsize_list)), R_pat_100_test_list, color=colors[1], linestyle=":",
                     label="{} test".format(100))
            plt.xticks(range(len(train_val_batchsize_list)), train_val_batchsize_list)
            plt.xlabel("train/val batchsize")
            plt.ylabel("recall")
            plt.title("Recall")
            plt.legend(loc="best")
            plt.show()

            # Train/val loss

            print()
            print("Loss...")

            for i in range(len(train_val_batchsize_list)):
                train_val_batchsize = train_val_batchsize_list[i]
                train_loss_pat_7 = np.loadtxt(SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_MB\\patience7\\"+str(train_val_batchsize[0])+"_"+str(train_val_batchsize[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")
                train_loss_pat_100 = np.loadtxt(SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_MB\\patience100\\" + str(train_val_batchsize[0]) + "_" + str(train_val_batchsize[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")
                val_loss_pat_7 = np.loadtxt(SAVE_VAL_LOSS_BASE_PATH + "\\BEST_MB\\patience7\\" + str(train_val_batchsize[0]) + "_" + str(train_val_batchsize[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")
                val_loss_pat_100 = np.loadtxt(SAVE_VAL_LOSS_BASE_PATH + "\\BEST_MB\\patience100\\" + str(train_val_batchsize[0]) + "_" + str(train_val_batchsize[1]) + "_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")

                plt.plot(range(len(train_loss_pat_7)), train_loss_pat_7, color=colors[i], linestyle="--",
                         label="({}) {} train".format(train_val_batchsize, 7))
                plt.plot(range(len(val_loss_pat_7)), val_loss_pat_7, color=colors[i], linestyle="-", label="({}) {} val".format(train_val_batchsize, 7))
                plt.plot(range(len(train_loss_pat_100)), train_loss_pat_100, color=colors[i], linestyle="--",
                         label="({}) {} train".format(train_val_batchsize, 100))
                plt.plot(range(len(val_loss_pat_100)), val_loss_pat_100, color=colors[i], linestyle="-",
                         label="({}) {} val".format(train_val_batchsize, 100))


            plt.legend(loc="best")
            plt.title("Train/Val loss: " + str(layers_width))
            plt.xlabel("n iter")
            plt.ylim(0,20)
            plt.ylabel("loss")
            plt.show()

            # ############# plot best model train VS best model train mb VS best model with Adam optimizer with and without variable lr VS SVM rbf kernel VS SVM linear kernel #############
            # APR
            APR_best_train = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_train, P_best_train, R_best_train = APR_best_train[0], APR_best_train[1], APR_best_train[2]
            APR_best_test = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_test, P_best_test, R_best_test = APR_best_test[0], APR_best_test[1], APR_best_test[2]

            APR_best_train_mb_SGD = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_MB\\patience100\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) +".out")
            A_best_train_mb_SGD, P_best_train_mb_SGD, R_best_train_mb_SGD = APR_best_train_mb_SGD[0], APR_best_train_mb_SGD[1], APR_best_train_mb_SGD[2]
            APR_best_test_mb_SGD = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_MB\\patience100\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) +".out")
            A_best_test_mb_SGD, P_best_test_mb_SGD, R_best_test_mb_SGD = APR_best_test_mb_SGD[0], APR_best_test_mb_SGD[1], APR_best_test_mb_SGD[2]

            APR_best_train_mb_adam_const_lr = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_const_lr_" + str(lr_adam)+".out")
            A_best_train_mb_adam_const_lr, P_best_train_mb_adam_const_lr, R_best_train_mb_adam_const_lr = APR_best_train_mb_adam_const_lr[0], APR_best_train_mb_adam_const_lr[1], APR_best_train_mb_adam_const_lr[2]
            APR_best_test_mb_adam_const_lr = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_const_lr_" + str(lr_adam)+".out")
            A_best_test_mb_adam_const_lr, P_best_test_mb_adam_const_lr, R_best_test_mb_adam_const_lr = APR_best_test_mb_adam_const_lr[0], APR_best_test_mb_adam_const_lr[1], APR_best_test_mb_adam_const_lr[2]

            APR_best_train_mb_adam_var_lr = np.loadtxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_var_lr"+".out")
            A_best_train_mb_adam_var_lr, P_best_train_mb_adam_var_lr, R_best_train_mb_adam_var_lr = APR_best_train_mb_adam_var_lr[0], APR_best_train_mb_adam_var_lr[1], APR_best_train_mb_adam_var_lr[2]
            APR_best_test_mb_adam_var_lr = np.loadtxt(SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_var_lr"+".out")
            A_best_test_mb_adam_var_lr, P_best_test_mb_adam_var_lr, R_best_test_mb_adam_var_lr = APR_best_test_mb_adam_var_lr[0], APR_best_test_mb_adam_var_lr[1], APR_best_test_mb_adam_var_lr[2]

            APR_best_train_svm_linear = np.loadtxt(
                SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_SVM_linear_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_train_svm_linear, P_best_train_svm_linear, R_best_train_svm_linear = APR_best_train_svm_linear[0], APR_best_train_svm_linear[1], APR_best_train_svm_linear[2]
            APR_best_test_svm_linear = np.loadtxt(
                SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_SVM_linear_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_test_svm_linear, P_best_test_svm_linear, R_best_test_svm_linear = APR_best_test_svm_linear[0], APR_best_test_svm_linear[1], APR_best_test_svm_linear[2]

            APR_best_train_svm_rbf = np.loadtxt(
                SAVE_A_P_R_BASE_TRAIN_PATH + "\\BEST_SVM_rbf_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_train_svm_rbf, P_best_train_svm_rbf, R_best_train_svm_rbf = APR_best_train_svm_rbf[0], APR_best_train_svm_rbf[1], APR_best_train_svm_rbf[2]
            APR_best_test_svm_rbf = np.loadtxt(
                SAVE_A_P_R_BASE_TEST_PATH + "\\BEST_SVM_rbf_" + str(len(layers_width)) + "_" + str(reg_factor) + ".out")
            A_best_test_svm_rbf, P_best_test_svm_rbf, R_best_test_svm_rbf = APR_best_test_svm_rbf[0], APR_best_test_svm_rbf[1], APR_best_test_svm_rbf[2]

            print()
            print("Performance (A, P, R)...")
            print("TRAIN (%17s | %20s | %20s | %20s | %20s | %20s )" % ("best train", "best train mb SGD", "best train mb Adam const lr", "best train mb Adam var lr", "best train SVM linear", "best train SVM rbf"))
            print("A = %20f | %20f | %20f | %20f | %20f | %20f" % (A_best_train, A_best_train_mb_SGD, A_best_train_mb_adam_const_lr, A_best_train_mb_adam_var_lr, A_best_train_svm_linear, A_best_train_svm_rbf))
            print("P = %20f | %20f | %20f | %20f | %20f | %20f" % (P_best_train, P_best_train_mb_SGD, P_best_train_mb_adam_const_lr, P_best_train_mb_adam_var_lr, P_best_train_svm_linear, P_best_train_svm_rbf))
            print("R = %20f | %20f | %20f | %20f | %20f | %20f" % (R_best_train, R_best_train_mb_SGD, R_best_train_mb_adam_const_lr, R_best_train_mb_adam_var_lr, R_best_train_svm_linear, R_best_train_svm_rbf))

            print("TEST (%18s | %20s | %20s | %20s | %20s | %20s )" % ("best test", "best test mb SGD", "best test mb Adam const lr", "best test mb Adam var lr", "best test SVM linear", "best test SVM rbf"))
            print("A = %20f | %20f | %20f | %20f | %20f | %20f" % (A_best_test, A_best_test_mb_SGD, A_best_test_mb_adam_const_lr, A_best_test_mb_adam_var_lr, A_best_test_svm_linear, A_best_test_svm_rbf))
            print("P = %20f | %20f | %20f | %20f | %20f | %20f" % (P_best_test, P_best_test_mb_SGD, P_best_test_mb_adam_const_lr, P_best_test_mb_adam_var_lr, P_best_test_svm_linear, P_best_test_svm_rbf))
            print("R = %20f | %20f | %20f | %20f | %20f | %20f" % (R_best_test, R_best_test_mb_SGD, R_best_test_mb_adam_const_lr, R_best_test_mb_adam_var_lr, R_best_test_svm_linear, R_best_test_svm_rbf))

            # loss
            colors = ["red", "green", "blue", "black"]
            train_loss_best = np.loadtxt(SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")
            val_loss_best = np.loadtxt(SAVE_VAL_LOSS_BASE_PATH + "\\BEST_" + str(len(layers_width)) + "_" + str(reg_factor) + ".txt")

            train_loss_best_mb_SGD = np.loadtxt(
                SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_MB\\patience100\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) +".txt")
            val_loss_best_mb_SGD = np.loadtxt(
                SAVE_VAL_LOSS_BASE_PATH + "\\BEST_MB\\patience100\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) +".txt")

            train_loss_best_mb_adam_const_lr = np.loadtxt(
                SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_const_lr_" + str(lr_adam)+".txt")
            val_loss_best_mb_adam_const_lr = np.loadtxt(
                SAVE_VAL_LOSS_BASE_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_const_lr_" + str(lr_adam)+".txt")

            train_loss_best_mb_adam_var_lr = np.loadtxt(
                SAVE_TRAIN_LOSS_BASE_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_var_lr"+".txt")
            val_loss_best_mb_adam_var_lr = np.loadtxt(
                SAVE_VAL_LOSS_BASE_PATH + "\\BEST_MB\\adam\\"+str(best_batch_size[0])+"_"+str(best_batch_size[1])+"_BEST_MB_" + str(len(layers_width)) + "_" + str(reg_factor) + "_var_lr"+".txt")

            plt.plot(range(len(train_loss_best)), train_loss_best, color=colors[0], linestyle="--", label="train")
            plt.plot(range(len(val_loss_best)), val_loss_best, color=colors[0], linestyle="-", label="val")

            plt.plot(range(len(train_loss_best_mb_SGD)), train_loss_best_mb_SGD, color=colors[1], linestyle="--", label="train mb SGD")
            plt.plot(range(len(val_loss_best_mb_SGD)), val_loss_best_mb_SGD, color=colors[1], linestyle="-", label="val mb SGD")

            plt.plot(range(len(train_loss_best_mb_adam_const_lr)), train_loss_best_mb_adam_const_lr, color=colors[2], linestyle="--", label="train mb Adam const lr")
            plt.plot(range(len(val_loss_best_mb_adam_const_lr)), val_loss_best_mb_adam_const_lr, color=colors[2], linestyle="-", label="val mb Adam const lr")

            plt.plot(range(len(train_loss_best_mb_adam_var_lr)), train_loss_best_mb_adam_var_lr, color=colors[3], linestyle="--", label="train mb Adam var lr")
            plt.plot(range(len(val_loss_best_mb_adam_var_lr)), val_loss_best_mb_adam_var_lr, color=colors[3], linestyle="-", label="val mb Adam var lr")

            plt.legend(loc="best")
            plt.title("Train/Val loss on best model: {} ({})".format(layers_width, best_batch_size))
            plt.xlabel("n iter")
            plt.ylim(0,5)
            plt.ylabel("loss")
            plt.show()

        ######## Random init model - LOSS
        if show_random_init_loss:
            print("Random init model - loss")
            for reg_factor in LAMBDAS:
                print()
                print("lambda =", reg_factor)
                for layers_width in layers_width_list:
                    ptd = pt_deep.PTDeep(layers_width_list[3], torch.relu).cuda()
                    print("{} loss = {}".format(layers_width, ptd.get_loss(x_train.reshape(N, -1).cuda(), Yoh_.cuda()).item()))
