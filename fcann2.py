import numpy as np
import matplotlib.pyplot as plt
import data

def stable_softmax_multi(scores, N, C):
    expscores = np.exp(scores - np.amax(scores, axis=1).reshape((N, -1)))  # N x C

    # nazivnik sofmaksa
    sumexp = np.sum(expscores, axis=1)  # N x 1

    probs = expscores / np.tile(sumexp.reshape((N, -1)), C)  # N x C

    return probs

def relu(scores):
    return np.maximum(0, scores)

def fcann2_train(X, Y_, H=5, param_niter=100000, param_delta=0.05, param_lambda=0.001):
    '''
        Argumenti
          X:  podatci, np.array NxD
          Y_: indeksi razreda, np.array NxC

        Povratne vrijednosti
          W, b: parametri logističke regresije
    '''

    C = (max(Y_) + 1)[0]
    N, D = X.shape
    Xt = X.T

    W1 = np.random.randn(H, D)   # H x D
    b1 = np.zeros((H, 1))     # H x 1
    W2 = np.random.randn(C, H)   # C x H
    b2 = np.zeros((C, 1))     # C x 1

    Yoh = data.get_one_hot_matrix(Y_.flatten(), N, C)

    # gradijentni spust (param_niter iteracija)
    for i in range(param_niter):
        # klasifikacijske mjere 1. sloja
        scores1 = (W1.dot(Xt) + b1).T  # N x H

        # latentne ReLU aktivacije 1. skrivenog sloja
        h1 = relu(scores1)  # N x H

        # klasifikacijske mjere 2. sloja
        scores2 = (W2.dot(h1.T) + b2).T  # N x C

        probs = stable_softmax_multi(scores2, N, C) # N x C

        # logaritmirane vjerojatnosti razreda
        logprobs = np.log(probs+1e-10)  # N x C
        logprobs_correct_class = (logprobs * Yoh).flatten()

        # gubitak
        loss = -1/N * np.sum(logprobs_correct_class) + param_lambda * (np.sum(np.square(W1)) + np.sum(np.square(W2)))  # scalar

        # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))
            # print("W",W)
            # print("b", b)
            # print()

        # derivacije gubitka po klasifikacijskim mjerama 2. sloja
        dL_dscores2 = probs - Yoh # N x C

        # gradijenti parametara 2. sloja
        grad_W2 = 1/N * np.dot(dL_dscores2.T, h1) + 2*param_lambda*W2 # C x H
        grad_b2 = np.sum(dL_dscores2.T, axis=1).reshape((C, 1))  # C x 1

        dL_dh1 = dL_dscores2.dot(W2)   # N x H

        dL_dscores1 = dL_dh1*(scores1>0)  # N x H

        grad_W1 = 1/N * np.dot(dL_dscores1.T, X) + 2*param_lambda*W1 # H x D
        grad_b1 = np.sum(dL_dscores1.T, axis=1).reshape((H, 1))  # H x 1

        # poboljšani parametri
        W2 += -param_delta * grad_W2
        b2 += -param_delta * grad_b2
        W1 += -param_delta * grad_W1
        b1 += -param_delta * grad_b1

    return W1, b1, W2, b2, loss

def fcann2_classify(C, X, W1, b1, W2, b2):
    '''
      Argumenti
          X:    podatci, np.array NxD
          W1, b1, W2, b2: parametri dubokog modela

      Povratne vrijednosti
          probs: vjerojatnosti razreda
    '''
    N = len(X)
    scores1 = (W1.dot(X.T) + b1).T  # N x H

    # latentne ReLU aktivacije 1. skrivenog sloja
    h1 = relu(scores1)  # N x H

    # klasifikacijske mjere 2. sloja
    scores2 = (W2.dot(h1.T) + b2).T  # N x C

    probs = stable_softmax_multi(scores2, N, C)  # N x C

    return probs

def fcann2_decfun(C, W1, b1, W2, b2):
    def classify(X):
        return fcann2_classify(C, X, W1, b1, W2, b2)
    return classify


if __name__ == "__main__":
    np.random.seed(100)

    data_whiten = False #True
    K = 6
    C = 2
    N = 10

    # zad 6
    # instantiate the dataset
    X, Y_ = data.sample_gmm_2d(K, C, N)

    if data_whiten:
        X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)

    # train the logistic regression model
    W1, b1, W2, b2, loss = fcann2_train(X, Y_, param_lambda=(0.0013 if data_whiten else 0.001))
    print("W1 =", W1)
    print("mean(W1) =", W1.mean())
    print("std(W1) =", W1.std())
    print("b1 =", b1)
    print("W2 =", W2)
    print("mean(W2) =", W2.mean())
    print("std(W2) =", W2.std())
    print("b2 =", b2)
    print("CE loss =", loss)

    # evaluate the model on the train set
    probs = fcann2_classify(C, X, W1, b1, W2, b2)

    # recover the predicted classes Y
    Y = np.argmax(probs, axis=1).reshape((len(X), -1))

    # # evaluate and print performance measures
    conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = data.eval_perf_multi(Y, Y_)
    # Yoh = get_one_hot_matrix(Y_.flatten(), N * C, C)
    # sorted_Y_by_pred_probs = Y_[(probs*Yoh).argsort(axis=0)[:,0]].flatten()
    print()
    print("confusion matrix =")
    for i in range(C):
        conf_mat_cls_i = conf_mats_by_cls[i]
        print("y =", i)
        print(conf_mat_cls_i)
    print("accuracy score =", accuracy_score)
    print("avg(accuracy) =", accuracy_avg)
    print("avg(precision) =", precision_avg)
    print("avg(recall) =", recall_avg)
    print("accuracy by classes =", accuracy_by_cls)
    print("precision by classes =", precision_by_cls)
    print("recall by classes =", recall_by_cls)

    # # graph the decision surface
    decfun = fcann2_decfun(C, W1, b1, W2, b2)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5, graph_multi=(C!=2), draw_cmap_multi=(C==2))

    # graph the data points
    data.graph_data(X, Y_, Y, C, graph_multi=True)

    # show the plot
    plt.show()