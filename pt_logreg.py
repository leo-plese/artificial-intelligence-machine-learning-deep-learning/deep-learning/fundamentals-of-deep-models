import torch.nn as nn
import torch
from torch.optim import SGD
import matplotlib.pyplot as plt
import data
import numpy as np


class PTLogreg(nn.Module):

    def __init__(self, D, C):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """
        super().__init__()

        # inicijalizirati parametre (koristite nn.Parameter):
        # imena mogu biti self.W, self.b
        # ...
        self.W = nn.Parameter(torch.randn(C, D))  # C x D
        self.b = nn.Parameter(torch.zeros((C, 1)))  # C x 1


    def forward(self, X):
        # unaprijedni prolaz modela: izračunati vjerojatnosti
        #   koristiti: torch.mm, torch.softmax
        # ...
        scores = (torch.mm(self.W, X.T) + self.b).T  # N x C
        probs = torch.softmax(scores, dim=1)

        return probs

    def get_loss(self, X, Yoh_, param_lambda=0):
        # formulacija gubitka
        #   koristiti: torch.log, torch.mean, torch.sum

        probs = self.forward(X)  # N x C
        # logaritmirane vjerojatnosti razreda
        logprobs = torch.log(probs+1e-10)  # N x C

        # gubitak
        logprobs_correct_class = (logprobs * Yoh_).flatten()
        loss = -1/len(X)*torch.sum(logprobs_correct_class) + param_lambda * (torch.sum(torch.square(self.W))) # scalar

        return loss


def train(model, X, Yoh_, param_niter, param_delta, param_lambda=0):
    """Arguments:
        - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
        - param_delta: learning rate
    """

    # inicijalizacija optimizatora
    optimizer = SGD(model.parameters(), lr=param_delta)#, weight_decay=param_lambda)

    # petlja učenja
    # ispisujte gubitak tijekom učenja
    for i in range(param_niter):
        loss = model.get_loss(X, Yoh_, param_lambda)

        loss.backward()

        optimizer.step()
        optimizer.zero_grad()

        # # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))
            # print("W",W)
            # print("b", b)
            # print()

    return model.W.detach().numpy(), model.b.detach().numpy(), loss.detach().numpy()


def eval(model, X):
    """Arguments:
        - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """
    # ulaz je potrebno pretvoriti u torch.Tensor
    # izlaze je potrebno pretvoriti u numpy.array
    # koristite torch.Tensor.detach() i torch.Tensor.numpy()

    X = torch.tensor(X, dtype=torch.float32)
    probs = model.forward(X)
    return probs.detach().numpy()

def logreg_decfun(model):
    def classify(X):
        return eval(model, X)
    return classify


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)
    torch.manual_seed(10)

    C = 2  # 3
    N = 100

    # instanciraj podatke X i labele Yoh_
    X, Y_ = data.sample_gauss_2d(C, N)

    data_whiten = True #False
    if data_whiten:
        X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)

    Yoh_ = data.get_one_hot_matrix(Y_.flatten(), len(X), (max(Y_) + 1)[0])

    # definiraj model:

    X_tensor = torch.tensor(X, dtype=torch.float32)
    Yoh_tensor = torch.tensor(Yoh_, dtype=torch.float32)
    #

    ptlr = PTLogreg(X.shape[1], Yoh_.shape[1])
    W, b, loss = train(ptlr, X_tensor, Yoh_tensor, 1000, 0.5, 0)

    print("W =", W)
    print("mean(W) =", W.mean())
    print("std(W) =", W.std())
    print("b =", b)
    print("CE loss =", loss)

    # dohvati vjerojatnosti na skupu za učenje
    probs = eval(ptlr, X)

    # ispiši performansu (preciznost i odziv po razredima)
    # recover the predicted classes Y
    Y = np.argmax(probs, axis=1).reshape((len(X), -1))

    # # evaluate and print performance measures
    conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = data.eval_perf_multi(Y, Y_)
    print()
    print("confusion matrix =")
    for i in range(C):
        conf_mat_cls_i = conf_mats_by_cls[i]
        print("y =", i)
        print(conf_mat_cls_i)
    print("accuracy score =", accuracy_score)
    print("avg(accuracy) =", accuracy_avg)
    print("avg(precision) =", precision_avg)
    print("avg(recall) =", recall_avg)
    print("accuracy by classes =", accuracy_by_cls)
    print("precision by classes =", precision_by_cls)
    print("recall by classes =", recall_by_cls)

    # iscrtaj rezultate, decizijsku plohu
    decfun = logreg_decfun(ptlr)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5, graph_multi=True)

    # graph the data points
    data.graph_data(X, Y_, Y, C, graph_multi=True)

    # show the plot
    plt.show()

    ##########################################################
    # za dane podatke, regularizacija
    LAMBDAS = [0, 1e-4, 1e-3, 1e-2, 1e-1, 2e-1, 3e-1, 5e-1, 6e-1, 7e-1, 8e-1, 1, 1.2, 1.4, 1.6, 1.8, 2]

    for param_lambda in LAMBDAS:
        np.random.seed(100)
        torch.manual_seed(10)
        ptlr = PTLogreg(X.shape[1], Yoh_.shape[1])
        print()
        print("//////////////////////////////////////////")
        print("lambda =", param_lambda)
        W, b, loss = train(ptlr, X_tensor, Yoh_tensor, 1000, 0.5, param_lambda)
        # premali N iter za dani lr
        # W, b, loss = train(ptlr, X_tensor, Yoh_tensor, 20, 0.5, param_lambda)
        # W, b, loss = train(ptlr, X_tensor, Yoh_tensor, 1000, 0.000001, param_lambda)
        # preveliki lr
        # W, b, loss = train(ptlr, X_tensor, Yoh_tensor, 1000, 30, param_lambda)


        print("W =", W)
        print("mean(W) =", W.mean())
        print("std(W) =", W.std())
        print("b =", b)
        print("CE loss =", loss)

        # dohvati vjerojatnosti na skupu za učenje
        probs = eval(ptlr, X)

        # ispiši performansu (preciznost i odziv po razredima)
        # recover the predicted classes Y
        Y = np.argmax(probs, axis=1).reshape((len(X), -1))

        # # evaluate and print performance measures
        conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = data.eval_perf_multi(Y, Y_)
        print()
        print("confusion matrix =")
        for i in range(C):
            conf_mat_cls_i = conf_mats_by_cls[i]
            print("y =", i)
            print(conf_mat_cls_i)
        print("accuracy score =", accuracy_score)
        print("avg(accuracy) =", accuracy_avg)
        print("avg(precision) =", precision_avg)
        print("avg(recall) =", recall_avg)
        print("accuracy by classes =", accuracy_by_cls)
        print("precision by classes =", precision_by_cls)
        print("recall by classes =", recall_by_cls)

        # iscrtaj rezultate, decizijsku plohu
        decfun = logreg_decfun(ptlr)
        bbox = (np.min(X, axis=0), np.max(X, axis=0))
        data.graph_surface(decfun, bbox, offset=0.5, graph_multi=True)

        # graph the data points
        data.graph_data(X, Y_, Y, C, graph_multi=True)

        plt.title("lambda = "+str(param_lambda) + "; A = " + str(accuracy_avg) + ", P = " + str(precision_avg) + ", R = " + str(recall_avg))
        # show the plot
        plt.show()

