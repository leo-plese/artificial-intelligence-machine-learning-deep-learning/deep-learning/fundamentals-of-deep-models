import numpy as np
import matplotlib.pyplot as plt

class Random2DGaussian():
    def __init__(self):
        self.minx = 0
        self.maxx = 10
        self.miny = 0
        self.maxy = 10

        mux = np.random.random_sample()*(self.maxx-self.minx)
        muy = np.random.random_sample() * (self.maxy - self.miny)
        self.mu = np.array([mux, muy])

        eigvalx = (np.random.random_sample()*(self.maxx-self.minx)/5)**2
        eigvaly = (np.random.random_sample() * (self.maxy - self.miny) / 5) ** 2

        D = np.diag(np.array([eigvalx, eigvaly]))

        phi = np.random.random_sample() * 2*np.pi
        R = np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])

        rt = R.T
        self.sigma = rt.dot(D).dot(R)
        return

    def get_sample(self, n):
        return np.random.multivariate_normal(self.mu, self.sigma, n)

def sample_gauss_2d(C, N):
    x, y = [], []
    rngs = []
    for i in range(C):
        rnd_2d_gauss_dist = Random2DGaussian()
        rngs.append(rnd_2d_gauss_dist)
        y.append([i for _ in range(N)])

    x = [g.get_sample(N) for g in rngs]
    X = np.reshape(x, (N*C, -1))
    Y_ = np.reshape(y, (N*C, -1))

    return X, Y_

def sample_gmm_2d(K, C, N):
    '''
    return:
      X  ... podatci u matrici [K·N x 2 ]
      Y_ ... indeksi razreda podataka [K·N]
    '''
    x, y = [], []
    rngs = []
    for i in range(K):
        rnd_2d_gauss_dist = Random2DGaussian()
        rngs.append(rnd_2d_gauss_dist)
        c_i = np.random.randint(C)
        y.append([c_i for _ in range(N)])

    x = [g.get_sample(N) for g in rngs]
    X = np.reshape(x, (N*K, -1))
    Y_ = np.reshape(y, (N*K, -1))

    return X, Y_

######################
def get_one_hot_matrix(cls_vect_Y_, N, C):
    Y_oh = np.zeros((N, C))
    Y_oh[np.arange(N), cls_vect_Y_] = 1
    return Y_oh

def myDummyDecision(X):
    scores = X[:,0] + X[:,1] - 5
    return scores

def graph_data(X, Y_, Y, C=2, graph_multi=False, special=[]):
    '''
      X  ... podatci (np.array dimenzija Nx2)
      Y_ ... točni indeksi razreda podataka (Nx1)
      Y  ... predviđeni indeksi razreda podataka (Nx1)
      C  ... broj klasa
    '''
    Y = np.reshape(Y, (len(Y_),-1))

    PT_SIZE = 20
    pt_sizes = np.repeat(PT_SIZE, len(Y))
    pt_sizes[special] = 2*PT_SIZE

    BOTTOM_COLOR_LIM = 0.3
    TOP_COLOR_LIM = 1

    if graph_multi:
        palette = np.arange(C) / (C - 1) * (TOP_COLOR_LIM-BOTTOM_COLOR_LIM) + BOTTOM_COLOR_LIM
    else:
        palette = [[BOTTOM_COLOR_LIM for _ in range(3)], [TOP_COLOR_LIM for _ in range(3)]]
    colors = np.zeros(shape=(len(Y),3))

    for i in range(len(palette)):
        colors[(Y_==i).flatten()] = palette[i]

    eq_inds = np.where(Y == Y_)[0]
    neq_inds = np.where(Y != Y_)[0]
    plt.scatter(X[eq_inds,0], X[eq_inds,1], s=pt_sizes[eq_inds], c=colors[eq_inds], edgecolors='k',marker='o')
    plt.scatter(X[neq_inds,0], X[neq_inds,1], s=pt_sizes[neq_inds], c=colors[neq_inds], edgecolors='k',marker='s')


def graph_surface(fun, rect, offset=0.5, width=256, height=256, graph_multi=False, draw_cmap_multi=False):
    '''
      fun    ... decizijska funkcija (Nx2)->(Nx1)
      rect   ... željena domena prikaza zadana kao:
                 ([x_min,y_min], [x_max,y_max])
      offset ... "nulta" vrijednost decizijske funkcije na koju
                 je potrebno poravnati središte palete boja;
                 tipično imamo:
                 offset = 0.5 za probabilističke modele
                    (npr. logistička regresija)
                 offset = 0 za modele koji ne spljošćuju
                    klasifikacijske mjere (npr. SVM)
      width,height ... rezolucija koordinatne mreže
    '''
    x_min, y_min = rect[0]
    x_max, y_max = rect[1]
    xx, yy = np.linspace(x_min, x_max, width), np.linspace(y_min, y_max, height)
    X, Y = np.meshgrid(xx, yy)

    coord = np.stack([X.flatten(), Y.flatten()], axis=1)    # axis=1 -> vertical matrix Nx2

    if draw_cmap_multi:
        dec_fun_vals = fun(coord)[:, 1]
    else:
        dec_fun_vals = fun(coord)
    if graph_multi:
        # 1. nacin: vjerojatnost neke klase
        # dec_fun_vals = dec_fun_vals[:,0].reshape((-1,1))    # OK
        ## dec_fun_vals = dec_fun_vals[:, 1].reshape((-1, 1))  # OK

        # 2. nacin: max vjerojatnost klasifikacije u bilo koju klasu
        # dec_fun_vals = np.amax(dec_fun_vals, axis=1).reshape((-1,1))      # OK

        # 3. nacin: klasa koja ima najvecu aposteriornu vjerojatnost
        dec_fun_vals = np.argmax(dec_fun_vals, axis=1).reshape((-1, 1))   # OK

    dec_fun_vals = np.reshape(dec_fun_vals, (width, height))  # njihov
    # dec_fun_vals = np.reshape(dec_fun_vals, (height, width))

    # # fix the range and offset
    maxval = max(np.max(dec_fun_vals) - offset, - (np.min(dec_fun_vals) - offset))

    # draw the surface and the offset
    plt.pcolormesh(X, Y, dec_fun_vals, vmin=offset - maxval, vmax=offset + maxval, cmap=plt.get_cmap('jet'), shading='auto')

    plt.contour(X, Y, dec_fun_vals, colors='k', levels=[offset])
#################

def get_accuracy(tp, tn, N):
    return (tp + tn) / N

def get_recall(tp, fn):
    return tp / (tp + fn)

def get_precision(tp, fp):
    if (tp + fp) == 0:
        return None
    return tp / (tp + fp)

# actual x predicted
# classes: from 0 to C-1 (top to bottom (predicted), left to right (actual))
def get_confusion_matrix(Y, Y_, C):
    conf_mat = np.zeros((C,C))
    for i in range(C):
        Y_pred_i = Y[np.where(Y_ == i)]

        for x in Y_pred_i:
            conf_mat[i][x] += 1

    conf_mat = conf_mat.T

    return conf_mat

# c_ind - class index
# classes: 0, 1 (top to bottom (predicted), left to right (actual))
def get_conf_mat_for_class(multi_conf_mat, c_ind, C):

    tp = multi_conf_mat[c_ind][c_ind]
    tn = sum([multi_conf_mat[i][j] if i != c_ind and j != c_ind else 0 for i in range(C) for j in range(C)])
    fp = sum([multi_conf_mat[c_ind][i] if i != c_ind else 0 for i in range(C)])
    fn = sum([multi_conf_mat[i][c_ind] if i != c_ind else 0 for i in range(C)])

    conf_mat = np.array([[tn,fn],[fp, tp]])

    return conf_mat

def eval_perf_multi(Y, Y_, N=None, C=None):
    if not C:
        C = max(Y_)[0] + 1  # 3
    if not N:
        N = len(Y_)
    conf_mat = get_confusion_matrix(Y.flatten(), Y_.flatten(), C)
    # print(conf_mat)

    conf_mats_by_cls = np.zeros((C, 2, 2))
    accuracy_by_cls, precision_by_cls, recall_by_cls = np.zeros(C), np.zeros(C), np.zeros(C)
    for i in range(C):
        conf_mat_for_class_i = get_conf_mat_for_class(conf_mat, i, C)
        conf_mats_by_cls[i] = conf_mat_for_class_i

        accuracy_by_cls[i] = get_accuracy(conf_mat_for_class_i[1][1], conf_mat_for_class_i[0][0], N)
        precision_by_cls[i] = get_precision(conf_mat_for_class_i[1][1], conf_mat_for_class_i[1][0])
        recall_by_cls[i] = get_recall(conf_mat_for_class_i[1][1], conf_mat_for_class_i[0][1])

    accuracy_avg = np.mean(accuracy_by_cls)
    precision_avg = np.mean(precision_by_cls)
    recall_avg = np.mean(recall_by_cls)

    accuracy_score = np.mean(Y == Y_)


    return conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls

def get_tp_tn_fp_fn_binary(Y, Y_):
    eq = (Y == Y_)
    neq = (Y != Y_)

    y0 = (Y == 0)
    y1 = (Y == 1)

    tp = np.sum(np.logical_and(eq, y1))
    tn = np.sum(np.logical_and(eq, y0))
    fp = np.sum(np.logical_and(neq, y1))
    fn = np.sum(np.logical_and(neq, y0))

    return tp, tn, fp, fn

def eval_perf_binary(Y,Y_):
    tp, tn, fp, fn = get_tp_tn_fp_fn_binary(Y, Y_)

    print("conf mat =")
    print(np.array([[tn, fn],[fp, tp]]))
    accuracy = get_accuracy(tp, tn, len(Y))
    recall = get_recall(tp, fn)
    precision = get_precision(tp, fp)

    return accuracy, recall, precision


def eval_AP(Y_):
    N = len(Y_)
    Y_ = np.array(Y_)
    gt_1_inds = np.where(Y_ == 1)[0]
    if len(gt_1_inds) == 0:
        return None

    precision_sum = 0
    for i in gt_1_inds:
        y0 = [0 for _ in range(i)]
        y1 = [1 for _ in range(i, N)]
        Y = np.array(y0 + y1)
        tp, _, fp, _ = get_tp_tn_fp_fn_binary(Y, Y_)
        precision_sum += get_precision(tp, fp)
    ap = precision_sum / len(gt_1_inds)

    return ap



if __name__ == "__main__":
    np.random.seed(100)

    # get data
    X, Y_ = sample_gmm_2d(4, 2, 30)

    # get the class predictions
    Y = myDummyDecision(X) > 0.5

    # graph the decision surface
    rect = (np.min(X, axis=0), np.max(X, axis=0))
    graph_surface(myDummyDecision, rect, offset=0)

    # graph the data points
    graph_data(X, Y_, Y)

    plt.show()