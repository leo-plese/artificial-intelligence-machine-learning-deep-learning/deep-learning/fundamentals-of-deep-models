Fundamentals of Deep Models. Tasks:

1 Generating linearly inseparable data

2 Multiclass classification in Python

3 Linear regression in PyTorch

4 Logistic regression in PyTorch

5 Configurable deep models in PyTorch

6 Comparison with SVM kernel

7 Case study: MNIST

Implemented in Python using NumPy, PyTorch, Matplotlib and scikit-learn library.

My lab assignment in Deep Learning, FER, Zagreb.

Created: 2021
