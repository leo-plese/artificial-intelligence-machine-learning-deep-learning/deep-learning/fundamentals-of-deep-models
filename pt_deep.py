import torch.nn as nn
import torch
from torch.optim import SGD, Adam
from torch.optim.lr_scheduler import ExponentialLR
import matplotlib.pyplot as plt
import data
import numpy as np
import mnist_shootout
from math import ceil


class PTDeep(nn.Module):

    def __init__(self, layer_width_list, f_activ):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """
        super().__init__()

        # inicijalizirati parametre (koristite nn.Parameter):
        # imena mogu biti self.W, self.b
        # ...
        n_wt_layers = len(layer_width_list)
        self.weights = nn.ParameterList(
            [nn.Parameter(torch.randn(layer_width_list[i], layer_width_list[i - 1])) for i in range(1, n_wt_layers)])
        self.biases = nn.ParameterList(
            [nn.Parameter(torch.zeros((layer_width_list[i], 1))) for i in range(1, n_wt_layers)])

        self.f = f_activ

    def count_params(self):
        num_params = 0
        for p in self.named_parameters():
            print(p[0] + " : " + str(list(p[1].shape)))
            num_params += torch.numel(p[1])
        return num_params

    def forward(self, X):
        # unaprijedni prolaz modela: izračunati vjerojatnosti
        #   koristiti: torch.mm, torch.softmax
        # ...
        scores = (torch.mm(self.weights[0], X.T) + self.biases[0]).T  # N x C
        for i in range(1, len(self.weights)):
            h = self.f(scores)
            scores = (torch.mm(self.weights[i], h.T) + self.biases[i]).T
        probs = torch.softmax(scores, dim=1)

        return probs

    def get_loss(self, X, Yoh_, param_lambda=0, get_data_loss_contrib_desc_order=False):
        # formulacija gubitka
        #   koristiti: torch.log, torch.mean, torch.sum

        probs = self.forward(X)  # N x C
        # logaritmirane vjerojatnosti razreda
        logprobs = torch.log(probs+1e-10)  # N x C

        # gubitak
        logprobs_correct_class = (logprobs * Yoh_).flatten()

        loss = -1 / len(X) * torch.sum(logprobs_correct_class) + param_lambda * torch.sum(
            torch.stack([torch.sum(torch.square(wts_i)) for wts_i in self.weights]))  # scalar

        if not get_data_loss_contrib_desc_order:
            return loss
        else:
            return loss, np.argsort(np.amax(-(logprobs*Yoh_).cpu().detach().numpy(),axis=1))[::-1]


def train(model, X, Yoh_, param_niter, param_delta, param_lambda=0, validate=False, X_val=None, y_val=None, Yoh_val=None, save_loss_path=None, get_data_loss_contrib=False, patience=None):
    """Arguments:
        - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
        - param_delta: learning rate
    """
    # inicijalizacija optimizatora
    optimizer = SGD(model.parameters(), lr=param_delta)  # , weight_decay=param_lambda)

    if validate:
        N_val, C = len(X_val), Yoh_val.shape[1]
        X_val_for_eval = X_val.reshape(N_val, -1)

        if patience:
            last_loss = float("inf")
            loss_counter = 0
            best_n_iter = param_niter

    losses_list, val_losses_list = [], []

    # petlja učenja
    # ispisujte gubitak tijekom učenja
    for i in range(param_niter):

        loss = model.get_loss(X, Yoh_, param_lambda)

        loss.backward()

        optimizer.step()
        optimizer.zero_grad()

        if validate:
            with torch.no_grad():
                val_loss = model.get_loss(X_val, Yoh_val, param_lambda).item()

            if patience:
                print("escnt", loss_counter)  # early stopping counter
                print("iteration {}: val loss {}".format(i, val_loss))

                if loss_counter == patience:
                    break

                val_losses_list.append(val_loss)

                cur_loss = val_loss
                if cur_loss < last_loss:
                    best_n_iter = i
                    last_loss = cur_loss
                    loss_counter = 0
                else:
                    loss_counter += 1
                    print("!!!!!!! iter {}: escnt = {}".format(i, loss_counter))
            else:
                val_losses_list.append(val_loss)

        # # dijagnostički ispis
        if i % 100 == 0:
            # print("iteration {}: loss {}".format(i, loss))

            if validate:
                print()
                print("---- iter", i)
                print("LOSS =", val_loss)

                with torch.no_grad():
                    A_val, P_val, R_val = mnist_shootout.evaluate_model(model, N_val, C, X_val_for_eval, y_val)

                print("A =", A_val)
                print("P =", P_val)
                print("R =", R_val)

        losses_list.append(loss)

    if get_data_loss_contrib:
        loss, data_loss_contrib_desc_order = model.get_loss(X, Yoh_, param_lambda, get_data_loss_contrib_desc_order=True)

    if patience:
        print("*********** best_n_iter =",best_n_iter, " ***********")
    if save_loss_path:
        np.savetxt(save_loss_path[0], np.array(losses_list))
        np.savetxt(save_loss_path[1], np.array(val_losses_list))

    if not get_data_loss_contrib:
        return [wts_i.cpu().detach().numpy() for wts_i in model.weights], [b_i.cpu().detach().numpy() for b_i in model.biases], loss.cpu().detach().numpy()
    else:
        return [wts_i.cpu().detach().numpy() for wts_i in model.weights], [b_i.cpu().detach().numpy() for b_i in model.biases], loss.cpu().detach().numpy(), data_loss_contrib_desc_order

def train_mb(model, X, Yoh_, param_nepoch, param_delta, param_lambda=0, train_batch_size=8, val_batch_size=2, X_val=None, y_val=None, Yoh_val=None, save_loss_path=None, get_data_loss_contrib=False, patience=None, optimizer=None, variable_lr=False):
    """Arguments:
        - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
        - param_delta: learning rate
    """
    N_train, N_val = len(X), len(X_val)
    X_val_for_eval = X_val.reshape(N_val, -1)

    train_num_batches = ceil(N_train / train_batch_size)
    val_num_batches = ceil(N_val / val_batch_size)

    scheduler = None
    # inicijalizacija optimizatora
    if not optimizer:
        optimizer = SGD(model.parameters(), lr=param_delta)  # , weight_decay=param_lambda)

    elif optimizer=="adam":
        optimizer = Adam(model.parameters(), lr=param_delta)
        if variable_lr:
            scheduler = ExponentialLR(optimizer, gamma=1-param_delta)

    if patience:
        last_loss = float("inf")
        loss_counter = 0
        best_n_epoch = param_nepoch

    losses_list, val_losses_list = [], []

    # petlja učenja
    # ispisujte gubitak tijekom učenja
    for epoch in range(param_nepoch):
        print("######################## EPOCH", epoch, " ########################")

        # train
        print("********* TRAIN *********")
        train_inds = torch.randperm(N_train)
        X, Yoh_ = X[train_inds], Yoh_[train_inds]

        train_loss = 0

        for i in range(train_num_batches):
            torch.cuda.empty_cache()
            data_from_ind, data_to_ind = i*train_batch_size, min((i+1)*train_batch_size, N_train)
            loss = model.get_loss(X[data_from_ind:data_to_ind], Yoh_[data_from_ind:data_to_ind], param_lambda)

            loss.backward()

            optimizer.step()
            optimizer.zero_grad()

            train_loss += loss/train_num_batches

            if i % 500 == 0:
                print("iter {}: {}".format(i, loss))

        # val
        print()
        print("********* VAL *********")
        val_inds = torch.randperm(N_val)
        X_val, Yoh_val = X_val[val_inds], Yoh_val[val_inds]

        val_loss = 0


        with torch.no_grad():
            for i in range(val_num_batches):
                torch.cuda.empty_cache()
                data_from_ind, data_to_ind = i * val_batch_size, min((i + 1) * val_batch_size, N_val)
                loss = model.get_loss(X_val[data_from_ind:data_to_ind], Yoh_val[data_from_ind:data_to_ind], param_lambda)

                val_loss += loss/val_num_batches

                if i % 500 == 0:
                    print("iter {}: {}".format(i, loss))

        if patience:
            print("escnt", loss_counter)  # early stopping counter
            if loss_counter == patience:
                break

            val_losses_list.append(val_loss)

            cur_loss = val_loss
            if cur_loss < last_loss:
                best_n_epoch = epoch
                last_loss = cur_loss
                loss_counter = 0
            else:
                loss_counter += 1
                print("!!!!!!! epoch {}: escnt = {}".format(epoch, loss_counter))
        else:
            val_losses_list.append(val_loss)

        losses_list.append(train_loss)

        print("epoch {}: train loss {}, val loss {}".format(epoch, train_loss, val_loss))


        with torch.no_grad():
            C = Yoh_val.shape[1]

            torch.cuda.empty_cache()
            A_val, P_val, R_val = mnist_shootout.evaluate_model(model, N_val, C, X_val_for_eval, y_val)

            print("A =", A_val)
            print("P =", P_val)
            print("R =", R_val)

        if scheduler:
            scheduler.step()

    if get_data_loss_contrib:
        loss, data_loss_contrib_desc_order = model.get_loss(X, Yoh_, param_lambda, get_data_loss_contrib_desc_order=True)

    if patience:
        print("*********** best_n_epoch =",best_n_epoch, " ***********")
    if save_loss_path:
        np.savetxt(save_loss_path[0], np.array(losses_list))
        np.savetxt(save_loss_path[1], np.array(val_losses_list))

    if not get_data_loss_contrib:
        return [wts_i.cpu().detach().numpy() for wts_i in model.weights], [b_i.cpu().detach().numpy() for b_i in model.biases], loss.cpu().detach().numpy()
    else:
        return [wts_i.cpu().detach().numpy() for wts_i in model.weights], [b_i.cpu().detach().numpy() for b_i in model.biases], loss.cpu().detach().numpy(), data_loss_contrib_desc_order


def eval(model, X):
    """Arguments:
        - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """
    # ulaz je potrebno pretvoriti u torch.Tensor
    # izlaze je potrebno pretvoriti u numpy.array
    # koristite torch.Tensor.detach() i torch.Tensor.numpy()

    X = torch.tensor(X, dtype=torch.float32)
    probs = model.forward(X)
    return probs.cpu().detach().numpy()


def logreg_decfun(model):
    def classify(X):
        return eval(model, X)

    return classify


def binlogreg_decfun(model):
    def classify(X):
        return eval(model, X)

    return classify


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)
    torch.manual_seed(10)

    data_whiten = True  # False
    #
    KCN_list = [(4, 2, 40), (6, 2, 10)]
    layers_width_list = [[2, 2], [2, 10, 2], [2, 10, 10, 2]]
    f_activ_list = [torch.relu, torch.sigmoid]

    for KCN in KCN_list:
        K, C, N = KCN
        print()
        print("/////////////////////////////////////////////")
        print("K C N =", K, C, N)
        X, Y_ = data.sample_gmm_2d(K, C, N)

        if data_whiten:
            X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)

        Yoh_ = data.get_one_hot_matrix(Y_.flatten(), len(X), (max(Y_) + 1)[0])

        X_tensor = torch.tensor(X, dtype=torch.float32)
        Yoh_tensor = torch.tensor(Yoh_, dtype=torch.float32)

        for layers_width in layers_width_list:
            print()
            print("layers_width =", layers_width)

            for f_activ in f_activ_list:
                np.random.seed(100)
                torch.manual_seed(10)

                print("--", f_activ)
                ptd = PTDeep(layers_width, f_activ)
                num_params = ptd.count_params()
                print("#params =", num_params)
                W, b, loss = train(ptd, X_tensor, Yoh_tensor, 1000, 0.5, 0)

                print("W =", W)
                print("b =", b)
                print("CE loss =", loss)

                # # dohvati vjerojatnosti na skupu za učenje
                # probs = eval(ptd, X)
                #
                # # ispiši performansu (preciznost i odziv po razredima)
                # # recover the predicted classes Y
                # Y = np.argmax(probs, axis=1).reshape((len(X), -1))
                #
                # # # evaluate and print performance measures
                # conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = data.eval_perf_multi(
                #     Y, Y_)
                # # Yoh = get_one_hot_matrix(Y_.flatten(), N * C, C)
                # # sorted_Y_by_pred_probs = Y_[(probs*Yoh).argsort(axis=0)[:,0]].flatten()
                # print()
                # print("confusion matrix =")
                # for i in range(C):
                #     conf_mat_cls_i = conf_mats_by_cls[i]
                #     print("y =", i)
                #     print(conf_mat_cls_i)
                # print("accuracy score =", accuracy_score)
                # print("avg(accuracy) =", accuracy_avg)
                # print("avg(precision) =", precision_avg)
                # print("avg(recall) =", recall_avg)
                # print("accuracy by classes =", accuracy_by_cls)
                # print("precision by classes =", precision_by_cls)
                # print("recall by classes =", recall_by_cls)
                #
                # # iscrtaj rezultate, decizijsku plohu
                # # # graph the decision surface
                # decfun = logreg_decfun(ptd)
                # bbox = (np.min(X, axis=0), np.max(X, axis=0))
                # # data.graph_surface(decfun, bbox, offset=0.5, graph_multi=True)
                # data.graph_surface(decfun, bbox, offset=0.5, draw_cmap_multi=True)
                #
                # # graph the data points
                # data.graph_data(X, Y_, Y, C, graph_multi=True)
                #
                # # show the plot
                # plt.show()

                # dohvati vjerojatnosti na skupu za učenje
                probs = eval(ptd, X)[:,1].reshape(K*N,-1)

                # ispiši performansu (preciznost i odziv po razredima)
                # recover the predicted classes Y
                Y = (probs >= 0.5)

                # evaluate and print performance measures
                accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
                sorted_Y_by_pred_probs = Y_[probs.argsort(axis=0)[:, 0]].flatten()
                AP = data.eval_AP(sorted_Y_by_pred_probs)
                print("accuracy =", accuracy)
                print("recall =", recall)
                print("precision =", precision)
                print("AP =", AP)

                # iscrtaj rezultate, decizijsku plohu
                # # graph the decision surface
                decfun = binlogreg_decfun(ptd)
                bbox = (np.min(X, axis=0), np.max(X, axis=0))
                data.graph_surface(decfun, bbox, offset=0.5, draw_cmap_multi=True)

                # graph the data points
                data.graph_data(X, Y_, Y)

                # show the plot
                plt.show()
# #
if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)
    torch.manual_seed(100)

    K = 6
    C = 2
    N = 10

    # instanciraj podatke X i labele Yoh_
    X, Y_ = data.sample_gmm_2d(K, C, N)

    data_whiten = True  #False
    if data_whiten:
        X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)

    Yoh_ = data.get_one_hot_matrix(Y_.flatten(), len(X), (max(Y_) + 1)[0])

    X_tensor = torch.tensor(X, dtype=torch.float32)
    Yoh_tensor = torch.tensor(Yoh_, dtype=torch.float32)

    ptlr = PTDeep([2,10, 10, 2], torch.relu)
    num_params = ptlr.count_params()
    print("#params =",num_params)
    W, b, loss = train(ptlr, X_tensor, Yoh_tensor, 10000, 0.1, 1e-4)

    print("W =", W)
    print("b =", b)
    print("CE loss =", loss)

    # dohvati vjerojatnosti na skupu za učenje
    probs = eval(ptlr, X)

    # ispiši performansu (preciznost i odziv po razredima)
    # recover the predicted classes Y
    Y = np.argmax(probs, axis=1).reshape((len(X), -1))

    # # evaluate and print performance measures
    conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = data.eval_perf_multi(Y, Y_)
    print()
    print("confusion matrix =")
    for i in range(C):
        conf_mat_cls_i = conf_mats_by_cls[i]
        print("y =", i)
        print(conf_mat_cls_i)
    print("accuracy score =", accuracy_score)
    print("avg(accuracy) =", accuracy_avg)
    print("avg(precision) =", precision_avg)
    print("avg(recall) =", recall_avg)
    print("accuracy by classes =", accuracy_by_cls)
    print("precision by classes =", precision_by_cls)
    print("recall by classes =", recall_by_cls)

    # iscrtaj rezultate, decizijsku plohu
    # # graph the decision surface
    decfun = logreg_decfun(ptlr)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    # data.graph_surface(decfun, bbox, offset=0.5, graph_multi=True)
    data.graph_surface(decfun, bbox, offset=0.5, draw_cmap_multi=True)

    # graph the data points
    data.graph_data(X, Y_, Y, C, graph_multi=True)

    # show the plot
    plt.show()


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)
    torch.manual_seed(10)

    C = 3  # 3
    N = 100

    # instanciraj podatke X i labele Yoh_
    X, Y_ = data.sample_gauss_2d(C, N)

    data_whiten = True#False
    if data_whiten:
        X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)

    Yoh_ = data.get_one_hot_matrix(Y_.flatten(), len(X), (max(Y_) + 1)[0])

    # definiraj model:
    ## W_init = torch.randn(Yoh_.shape[1], X.shape[1])  # C x D
    ## b_init = torch.zeros((Yoh_.shape[1], 1))  # C x 1

    X_tensor = torch.tensor(X, dtype=torch.float32)
    Yoh_tensor = torch.tensor(Yoh_, dtype=torch.float32)
    #

    ptlr = PTDeep([2,C], torch.relu)
    num_params = ptlr.count_params()
    print("#params =",num_params)
    W, b, loss = train(ptlr, X_tensor, Yoh_tensor, 1000, 0.5, 0)

    print("W =", W)
    # print("mean(W) =", W.mean())
    # print("std(W) =", W.std())
    print("b =", b)
    print("CE loss =", loss)

    # dohvati vjerojatnosti na skupu za učenje
    probs = eval(ptlr, X)

    # ispiši performansu (preciznost i odziv po razredima)
    # recover the predicted classes Y
    Y = np.argmax(probs, axis=1).reshape((len(X), -1))

    # # evaluate and print performance measures
    conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = data.eval_perf_multi(
        Y, Y_)
    # Yoh = get_one_hot_matrix(Y_.flatten(), N * C, C)
    # sorted_Y_by_pred_probs = Y_[(probs*Yoh).argsort(axis=0)[:,0]].flatten()
    print()
    print("confusion matrix =")
    for i in range(C):
        conf_mat_cls_i = conf_mats_by_cls[i]
        print("y =", i)
        print(conf_mat_cls_i)
    print("accuracy score =", accuracy_score)
    print("avg(accuracy) =", accuracy_avg)
    print("avg(precision) =", precision_avg)
    print("avg(recall) =", recall_avg)
    print("accuracy by classes =", accuracy_by_cls)
    print("precision by classes =", precision_by_cls)
    print("recall by classes =", recall_by_cls)

    # iscrtaj rezultate, decizijsku plohu
    # # graph the decision surface
    decfun = logreg_decfun(ptlr)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    # data.graph_surface(decfun, bbox, offset=0.5, graph_multi=True)
    data.graph_surface(decfun, bbox, offset=0.5, graph_multi=True)

    # graph the data points
    data.graph_data(X, Y_, Y, C, graph_multi=True)

    # show the plot
    plt.show()
