import torch
import torch.nn as nn
import torch.optim as optim

if __name__=="__main__":
    ## Definicija računskog grafa
    # podaci i parametri, inicijalizacija parametara
    a = torch.randn(1, requires_grad=True)
    b = torch.randn(1, requires_grad=True)

    X = torch.tensor([1, 2, 3])
    Y = torch.tensor([3, 5, 7])

    N = len(X)

    dL_da = lambda Y_pred : -2 * torch.mean(X * (Y - Y_pred))
    dL_db = lambda Y_pred : -2 * torch.mean((Y - Y_pred))

    # optimizacijski postupak: gradijentni spust
    optimizer = optim.SGD([a, b], lr=0.1)

    for i in range(100):
        # afin regresijski model
        Y_ = a*X + b

        diff = (Y-Y_)

        # kvadratni gubitak
        loss = torch.mean(diff ** 2)    # neovisno o broju podataka - radi i za > 2 podatka

        dL_da_grad = dL_da(Y_)
        dL_db_grad = dL_db(Y_)
        # računanje gradijenata
        loss.backward()

        print("a._grad =", a._grad, " : ", dL_da_grad)
        print("b._grad =", b._grad, " : ", dL_db_grad)

        # korak optimizacije
        optimizer.step()

        # Postavljanje gradijenata na nulu
        optimizer.zero_grad()

        print(f'step: {i}, loss:{loss}, Y_:{Y_}, a:{a}, b {b}')